import argparse, subprocess

parser = argparse.ArgumentParser(description='For those too lazy to type docker-compose in the shell.')
parser.add_argument(
    '-w', '--which',
    help='which docker-compose to spin up',
    choices=['ai', 'web'],
    type=str,
    default='web'
)

parser.add_argument(
    '-p', '--production',
    help='mode to spin docker up in',
    action="store_true",
    default=False
)

parser.add_argument(
    '-c', '--command',
    help='what to do with docker-compose',
    choices=['build', 'up', 'down'],
    type=str,
    default='up'
)


args = parser.parse_args()

command = ['docker-compose']
if args.which == 'web':
    command.append('-f')
    command.append('docker-compose.{}.production.yml'.format(args.which))
    if args.production == False:
        command.append('-f')
        command.append('docker-compose.{}.development.yml'.format(args.which))

if args.which == 'ai':
    command.append('-f')
    command.append('docker-compose.{}.development.yml'.format(args.which))

command.append(args.command)
subprocess.run(command)
